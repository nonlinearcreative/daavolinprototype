﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace DeepCanvas
{
	public class SkinPatch : MonoBehaviour {

		GameObject uvLamp;
		GameObject skinPatch;
		Material skinPatchMat;

		public Material eczemaSkinMat;
		public Material psoriasisSkinMat;
		public Material vitiligoSkinMat;



		public void Init()
		{
			uvLamp = this.gameObject.transform.GetChild(0).gameObject; 
			uvLamp.SetActive(false);
			skinPatch = gameObject;
			skinPatchMat = skinPatch.GetComponent<Renderer>().material;
		}

	
	
		public void FadePatchOut()
		{
			skinPatchMat = skinPatch.GetComponent<Renderer>().material;
			skinPatchMat.DOColor(new Color(skinPatchMat.color.r, skinPatchMat.color.g, skinPatchMat.color.b, 0), 15f).OnComplete(KillLamp);
			uvLamp.SetActive(true);
		}
		void KillLamp()
		{
			uvLamp.SetActive(false);
		}

		public void ChangeSkinConditionMaterial(EnumLibrary.SkinConditions condition)
		{
			switch (condition)
			{
			case EnumLibrary.SkinConditions.ECZEMA:
				gameObject.GetComponent<Renderer>().material = eczemaSkinMat;
				Debug.Log(condition);
				break;

			case EnumLibrary.SkinConditions.PSORIASIS:
				gameObject.GetComponent<Renderer>().material = psoriasisSkinMat;
				Debug.Log(condition);
				break;

			case EnumLibrary.SkinConditions.VITILIGO:
				gameObject.GetComponent<Renderer>().material = vitiligoSkinMat;
				Debug.Log(condition);
				break;
			}
			skinPatchMat.color = new Color(skinPatchMat.color.r, skinPatchMat.color.g, skinPatchMat.color.b, 1);
		}
			

	}
}
