﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DeepCanvas
{
	public class SceneManager : MonoBehaviour {

		AnimationManager animMan;
		UIManager uiMan;
		bool leftUIIsShowing;

		void Start()
		{
			leftUIIsShowing = false;
			uiMan = GetComponent<UIManager>();
			uiMan.Init();
			animMan = GetComponent<AnimationManager>();
			animMan.Init();
		}

		public void ShowHideUIPanel() // toggles UI panel on left arm
		{
			if(leftUIIsShowing)
				uiMan.ShowHideLeftUIPanel(false);
			else
				uiMan.ShowHideLeftUIPanel(true);
			leftUIIsShowing = !leftUIIsShowing;
		}

		public void ShowHideCube(bool showSample){animMan.ToggleCubeInOut(showSample);} // toggles sample cube in and out

		public void ChangeSampleCubeTexture(EnumLibrary.SkinConditions type)
		{
			uiMan.DisplayInformation(type); // sets UI to the current type

			switch(type)
			{
			case EnumLibrary.SkinConditions.ECZEMA:
				animMan.SelectEczema();
				break;

			case EnumLibrary.SkinConditions.PSORIASIS:
				animMan.SelectPsoriasis();
				break;

			case EnumLibrary.SkinConditions.VITILIGO:
				animMan.SelectVitiligo();
				break;
			}
		}

		public void BeginTreatment(){animMan.BeginUVTreatment();}


		void Update()
		{
			if(Input.GetKeyDown(KeyCode.Space))
			{
				Application.Quit();
			}
		}
	}
}
