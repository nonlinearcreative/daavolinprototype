﻿using UnityEngine;
using UnityEngine.UI;

namespace DeepCanvas
{
	public class ToggleButton : MonoBehaviour {

		SceneManager sceneMan;
		Toggle t;

		void Start () 
		{
			sceneMan = GameObject.FindWithTag("Manager").GetComponent<SceneManager>();
			t = GetComponent<Toggle>();
			t.onValueChanged.AddListener((value) => CheckboxHandler(value));
		}

		void CheckboxHandler (bool value) 
		{
			//Debug.Log(value);
			sceneMan.ShowHideCube(value);
		}
	}
}
