﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class CanvasFader : MonoBehaviour {

	public bool f = true;

	public float fadeTime = 1f;
	float fadeValue =  0;



	public void FadeMeOut()
	{
		DOTween.To(()=> fadeValue, x=> fadeValue = x, 0, 2);

		if (f) 
		{
			
			//StartCoroutine ("FadeOut");
		}
	}


	public void FadeMeIn()
	{
		DOTween.To(()=> fadeValue, x=> fadeValue = x, 1, fadeTime);
	}



	void Update()
	{
		//Debug.Log (fadeValue);
		if (Input.GetKeyDown (KeyCode.T)) 
		{
			FadeMeIn ();
		}
		if (Input.GetKeyDown (KeyCode.Y)) 
		{
			FadeMeOut ();
		}
	}

	// The new way

	public void StartFadeRoutine()
	{
		DOTween.To(()=> fadeValue, x=> fadeValue = x, 1, fadeTime).OnComplete(ManagerMoveCamera);
	}

	void ManagerMoveCamera()
	{
	}


	/* The old way  (sucks)
	IEnumerator FadeOut()
	{
		CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
		while (canvasGroup.alpha>0)
		{
			f = false;
			canvasGroup.alpha -= Time.deltaTime;
			if(canvasGroup.alpha<=0)
			{
				canvasGroup.alpha=0;
				f = true;
			}
			yield return null;
		}
		canvasGroup.interactable = false;
		yield return null;
	}

	IEnumerator FadeIn()
	{
		CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
		while (canvasGroup.alpha<1)
		{
			f = false; 
			canvasGroup.alpha += Time.deltaTime;
			if(canvasGroup.alpha>=1)
			{
				canvasGroup.alpha=1;
				f = true;
			}
			yield return null;
		}

		canvasGroup.interactable = true;
		yield return null;
	}
	*/

	public void CanvasGroupInvisible()
	{
		CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
		canvasGroup.alpha = 0;
		canvasGroup.interactable = false;
	}

}
