﻿using UnityEngine;

namespace DeepCanvas
{
	public class UIManager : MonoBehaviour {

		public GameObject leftHandUIPanel;

		public GameObject eczemaInfoPanel;
		public GameObject psoriasisInfoPanel;
		public GameObject vitiligoTypePanel;

		public void Init()
		{
			DisplayInformation(EnumLibrary.SkinConditions.PSORIASIS); // Make psoriasis default starting state
			AllUIPanelsOff(); // first shut off the children
			leftHandUIPanel.SetActive(false); // then the parent
		}

		public void ShowHideLeftUIPanel(bool isShowing){leftHandUIPanel.SetActive(isShowing);} // Left arm UI toggle

		public void DisplayInformation(EnumLibrary.SkinConditions info) // info panel toggle
		{
			AllUIPanelsOff();
			switch (info)
			{
			case EnumLibrary.SkinConditions.ECZEMA:
				eczemaInfoPanel.SetActive(true);
				break;

			case EnumLibrary.SkinConditions.PSORIASIS:
				psoriasisInfoPanel.SetActive(true);
				break;

			case EnumLibrary.SkinConditions.VITILIGO:
				vitiligoTypePanel.SetActive(true);
				break;
			}
		}


		void AllUIPanelsOff()
		{
			eczemaInfoPanel.SetActive(false);
			psoriasisInfoPanel.SetActive(false);
			vitiligoTypePanel.SetActive(false);
		}
	}
}
