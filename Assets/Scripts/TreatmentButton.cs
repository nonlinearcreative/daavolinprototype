﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DeepCanvas
{
	public class TreatmentButton : MonoBehaviour {

		SceneManager sceneMan;
		Button thisButton;

		void Start () 
		{
			sceneMan = GameObject.FindWithTag("Manager").GetComponent<SceneManager>();	
			thisButton = GetComponent<Button>();
			thisButton.onClick.AddListener(() => ButtonClicked());
		}

		public void ButtonClicked(){sceneMan.BeginTreatment();} // ... it gets passed to the manager here.

	}
}
