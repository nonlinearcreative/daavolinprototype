﻿using UnityEngine;
using UnityEngine.EventSystems;


namespace DeepCanvas
{
	public class AnimationManager : MonoBehaviour 
	{
		public Animator leftArmAnimator;
		Animation leftArmAnim;
		public SkinPatch skinPatch;

		public GameObject cubeSample;
		public bool cubeIsShowing;

		public Material eczemaMat;
		public Material psoriasisMat;
		public Material vitiligoMat;

		public void Init()
		{
			leftArmAnim = leftArmAnimator.gameObject.GetComponent<Animation>();
			skinPatch.Init();
			cubeIsShowing = false;
		}

		public void ToggleCubeInOut(bool c)
		{
			cubeIsShowing = c;

			if(cubeIsShowing)
			{
				leftArmAnimator.SetBool("cubeIsOut", true);
			}
			else
			{
				leftArmAnimator.SetBool("cubeIsOut", false);
			}

		}

		public void SelectEczema()
		{
			cubeSample.GetComponent<Renderer>().material = eczemaMat;
			skinPatch.ChangeSkinConditionMaterial(EnumLibrary.SkinConditions.ECZEMA);
		}
		public void SelectPsoriasis()
		{
			cubeSample.GetComponent<Renderer>().material = psoriasisMat;
			skinPatch.ChangeSkinConditionMaterial(EnumLibrary.SkinConditions.PSORIASIS);
		}
		public void SelectVitiligo()
		{
			cubeSample.GetComponent<Renderer>().material = vitiligoMat;
			skinPatch.ChangeSkinConditionMaterial(EnumLibrary.SkinConditions.VITILIGO);
		}

		public void BeginUVTreatment()
		{
			skinPatch.FadePatchOut(); // begins the treatment
		}

	}
}
