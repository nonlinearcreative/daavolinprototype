﻿using UnityEngine;
using UnityEngine.UI;

namespace DeepCanvas
{
	public class VRButtonListener : MonoBehaviour {

		public EnumLibrary.SkinConditions thisButtonsSkinCondition;  // set this in inspector...
		SceneManager sceneMan;
		Button thisButton;

		void Start () 
		{
			sceneMan = GameObject.FindWithTag("Manager").GetComponent<SceneManager>();	
			thisButton = GetComponent<Button>();
			thisButton.onClick.AddListener(() => ButtonClicked());
		}

		public void ButtonClicked(){sceneMan.ChangeSampleCubeTexture(thisButtonsSkinCondition);} // ... it gets passed to the manager here.
	}
}
