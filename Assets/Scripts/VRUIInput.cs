﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DeepCanvas
{

[RequireComponent(typeof(SteamVR_LaserPointer))]

	public class VRUIInput : MonoBehaviour
	{
		//SceneManager manager;
		private SteamVR_LaserPointer laserPointer;
		private SteamVR_TrackedController trackedController;


		void Start()
		{
			//manager = GameObject.FindWithTag("Manager").GetComponent<SceneManager>();
		}

		private void OnEnable()
		{
			//manager = GameObject.FindWithTag("Manager").GetComponent<SceneManager>();
			laserPointer = GetComponent<SteamVR_LaserPointer>();
			laserPointer.PointerIn -= HandlePointerIn;
			laserPointer.PointerIn += HandlePointerIn;
			laserPointer.PointerOut -= HandlePointerOut;
			laserPointer.PointerOut += HandlePointerOut;

			trackedController = GetComponent<SteamVR_TrackedController>();
			if (trackedController == null)
			{
				trackedController = GetComponentInParent<SteamVR_TrackedController>();
			}
			trackedController.TriggerClicked -= HandleTriggerClicked;
			trackedController.TriggerClicked += HandleTriggerClicked;
		}

		private void HandleTriggerClicked(object sender, ClickedEventArgs e)
		{
			if (EventSystem.current.currentSelectedGameObject != null)
			{
				ExecuteEvents.Execute(EventSystem.current.currentSelectedGameObject, new PointerEventData(EventSystem.current), ExecuteEvents.submitHandler);
			}
		}

		private void HandlePointerIn(object sender, PointerEventArgs e)
		{
			var button = e.target.GetComponent<Button>();
			if (button != null)
			{
				button.Select();
				//Debug.Log("HandlePointerIn", e.target.gameObject);
			}
			var toggle = e.target.GetComponent<Toggle>();
			if(toggle != null)
			{
				toggle.Select();
			}
		}

		private void HandlePointerOut(object sender, PointerEventArgs e)
		{

			var button = e.target.GetComponent<Button>();
			if (button != null)
			{
				EventSystem.current.SetSelectedGameObject(null);
				//Debug.Log("HandlePointerOut", e.target.gameObject);
			}

			var toggle = e.target.GetComponent<Toggle>();
			if (toggle != null)
			{
				EventSystem.current.SetSelectedGameObject(null);
				//Debug.Log("HandlePointerOut", e.target.gameObject);
			}
		}
	}
}