﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DeepCanvas
{
	public class ViveControllerBehavior : MonoBehaviour 
	{

		SceneManager manager;
		private SteamVR_TrackedObject trackedObject;
		private SteamVR_Controller.Device device;
		private SteamVR_TrackedController controller;
		float xVal;
		float yVal;

		void Start()
		{
			trackedObject = GetComponent<SteamVR_TrackedObject>();
			controller = GetComponent<SteamVR_TrackedController>();
			controller.PadClicked += Controller_PadClicked;
			manager = GameObject.FindWithTag("Manager").GetComponent<SceneManager>();
		}


		void Update()
		{
			device = SteamVR_Controller.Input((int)trackedObject.index);
		}

		private void Controller_PadClicked(object sender, ClickedEventArgs e)
		{
			manager.ShowHideUIPanel(); // Tells manager to toggle menu on and off

			// for using quadrants on the vive button   |
			//											|
			//											|
			//											V

			/*
			if(device.GetAxis().x !=0 || device.GetAxis().y !=0)
			{
				//Debug.Log(device.GetAxis().x + " " + device.GetAxis().y);
				xVal = device.GetAxis().x;
				yVal = device.GetAxis().y;

				if(xVal > 0 && yVal > 0)
				{
					//Debug.Log("Upper right quadrant.");
					manager.ChangeSampleCubeTexture(EnumLibrary.SkinConditions.ECZEMA);
				}

				if(xVal < 0 && yVal > 0)
				{
					//Debug.Log("Upper left quadrant.");
					manager.ChangeSampleCubeTexture(EnumLibrary.SkinConditions.PSORIASIS);
				}

				if(xVal < 0 && yVal < 0)
				{
					//Debug.Log("Lower left quadrant.");
				}

				if(xVal > 0 && yVal < 0)
				{
					//Debug.Log("Lower right quadrant.");
					manager.ChangeSampleCubeTexture(EnumLibrary.SkinConditions.VITILIGO);
				}

				
			}
*/
		}
	}
}
		

	

